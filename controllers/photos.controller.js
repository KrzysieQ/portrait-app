const requestIp = require('request-ip');
const Photo = require('../models/photo.model');
const Voter = require('../models/voter.model');

/****** SUBMIT PHOTO ********/

exports.add = async (req, res) => {

  try {
    const { title, author, email } = req.fields;
    const file = req.files.file;

    if(title && author && email && file) { // if fields are not empty...

      const fileName = file.path.split('/').slice(-1)[0]; // cut only filename from full path, e.g. C:/test/abc.jpg -> abc.jpg
      const fileExt = fileName.split('.').slice(-1)[0];
      if(fileExt !== 'gif' && fileExt !== 'jpg' && fileExt !== 'png') throw new Error('Invalid file extension!');
      if(title.length > 25 || author.length > 50) throw new Error(`Title or author is too long. Max length for title is 25 characters and for author is 50!`);
      const regexTags = /<.+>(.*)<\/.+>/g
      const escapedTitle = title.replace(regexTags, '$1')
      const newPhoto = new Photo({ title: escapedTitle, author, email, src: fileName, votes: 0 });
      await newPhoto.save(); // ...save new photo in DB
      res.json(newPhoto);
    } else {
      throw new Error('Wrong input!');
    }

  } catch(err) {
    res.status(500).json(err);
  }

};

/****** LOAD ALL PHOTOS ********/

exports.loadAll = async (req, res) => {

  try {
    res.json(await Photo.find());
  } catch(err) {
    res.status(500).json(err);
  }

};

/****** VOTE FOR PHOTO ********/

exports.vote = async (req, res) => {

  try {
    const userIp = requestIp.getClientIp(req);
    const photoToUpdate = await Photo.findOne({ _id: req.params.id });
    if(!photoToUpdate) res.status(404).json({ message: 'Not found' });
    else {
      photoToUpdate.votes++;
      photoToUpdate.save();
    }
    let voter = await Voter.findOne({ user: userIp });
    if(!voter) voter = await Voter.findOne({ user: userIp, votes: req.params.id });
    else if(voter.votes.includes(req.params.id)) res.status(400).json({ message: 'Already voted!' });
    else {
      voter.votes.push(req.params.id)
      voter.save();
    }
    res.send({ message: 'OK' });
  } catch(err) {
    res.status(500).json(err);
  }

};
